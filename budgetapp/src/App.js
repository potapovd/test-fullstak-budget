import React, {Component} from 'react';
import accounting from 'accounting';
import Moment from 'react-moment';
import moment from 'moment';
import logo from './logo.svg';
import './App.css';

class App extends Component {
	constructor() {
		super();
		this.state = {
			title: "Welcome to BudgetApp",
			budgetArr: [],
			summ: 0,
			datefilter: "",
			formaction: "new",
			type: "debit",
			date: "",
			name: "",
			amount: 0,
			id: null

		}
		this.sumCalc = this.sumCalc.bind(this);
		this.getFullBudget = this.getFullBudget.bind(this);
		this.checkValidData = this.checkValidData.bind(this);
	}


	//calculate TOTAL SUMM
	sumCalc() {
		let {budgetArr} = this.state;
		let summ = 0;
		for (let i = 0; i < budgetArr.length; i++) {
			if (budgetArr[i].type === "credit") {
				summ = summ - Number(budgetArr[i].amount);
			} else {
				summ = summ + Number(budgetArr[i].amount);
			}
		}
		this.setState({summ: summ});
		console.log(`Tolal Summ: ${summ}`);
	}

	//simplechack data
	checkValidData(data) {
		console.log(typeof (data.name));

		if (
			data.id > 0 ||
			data.amount > 0
		) {
			console.log("DATA IS VALID");
			return true
		} else {
			console.log("DATA IS INVALID");
			return false

		}
	}

	//query get all lines
	getFullBudget() {
		fetch('http://localhost:3000/api/getData', {
			cache: 'no-cache',
			mode: 'cors'
		})
			.then(
				response => {
					if (response.ok) {
						return response.json()
					} else {
						return Promise.reject('something went wrong!')
					}
				}
			)
			.then(data => {
				console.log(`Tolal lines: ${data.length}`);
				if (data.length > 0) {
					let newLine = [];
					for (let i = 0; i < data.length; i++) {
						newLine.push(data[i]);
					}
					this.setState({budgetArr: newLine}, () => this.sumCalc())
				}
			})
			.catch(
				error => {
					console.log(`ERROR getData - ${error}`);
				}
			)
	}

	componentDidMount() {
		//get all lines
		this.getFullBudget();
	}

	//ADD new and EDIT  exist. line
	handleAddNewLine(e) {
		e.preventDefault();

		if (this.state.formaction == "new") {

			//ADD NEW LINE
			let lineData = {
				type: this.refs.type.value,
				date: this.refs.date.value,
				name: this.refs.name.value,
				amount: this.refs.amount.value,
				id: Math.random().toFixed(2) * 100
			}

			//valid checker
			if (this.checkValidData(lineData)) {
				const request = new Request('http://localhost:3000/api/newline', {
					method: 'POST',
					headers: new Headers({
						'Content-Type': 'application/json'
					}),
					body: JSON.stringify(lineData)
				})

				fetch(request).then(
					response => {
						return response.json()
					}
				)
					.then(data => {
						let newLine = this.state.budgetArr.slice();
						newLine.push(lineData);
						this.setState({budgetArr: newLine}, () => {
							this.sumCalc();
						})
					})
					.catch(
						error => {
							console.log(`ERROR handleAddNewLine (ADD) - ${error}`);
						}
					)
			}

		} else {
			//EDIT LINE

			let lineData = {
				id: this.state.id,
				name: this.state.name,
				type: this.state.type,
				date: this.state.date,
				amount: this.state.amount
			}

			//valid checker
			if (this.checkValidData(lineData)) {
				const request = new Request('http://localhost:3000/api/editline/' + this.state.id, {
					method: 'PUT',
					headers: new Headers({
						'Content-Type': 'application/json'
					}),
					body: JSON.stringify(lineData)
				})

				fetch(request).then(
					response => {
						return response.json()
					}
				)
					.then(data => {
						//console.log(data);
						this.setState({
							formaction: "new",
							type: "debit",
							date: "",
							name: "",
							amount: "",
							id: null
						})

					})
					.catch(
						error => {
							console.log(`ERROR handleAddNewLine (EDIT) - ${error}`);
						}
					)
			}

		}

	}

	//REMOVE line
	handleRemoveLine(id) {
		console.log(`REMOVING ${id}`);
		const request = new Request('http://localhost:3000/api/remove/' + id, {
			method: 'DELETE',
		});
		fetch(request).then(
			response => {
				return response.json()
			}
		)
			.then(data => {
				let newBudget = this.state.budgetArr.slice();
				for (let i = 0; i < newBudget.length; i++) {
					if (id === newBudget[i].id) {
						newBudget.splice(i, 1)
					}
				}
				this.setState({budgetArr: newBudget}, () => {
					this.sumCalc();
				})
			})
			.catch(
				error => {
					console.log(`ERROR remove - ${error}`);
				}
			)
	}

	//EDIT line
	handleEditLine(id) {
		let line = this.state.budgetArr.filter((budgetLine) => {
			if (budgetLine.id === id) return true
		})
		this.setState({
			formaction: "edit",
			id: line[0].id,
			name: line[0].name,
			type: line[0].type,
			date: line[0].date,
			amount: line[0].amount
		})
	}

	handleChangeLine(e) {
		e.target.name === 'amount' ?
			(this.setState({[e.target.name]: parseInt(e.target.value)}, () => {
				for (let i = 0; i < this.state.budgetArr.length; i++) {
					if (this.state.id === this.state.budgetArr[i].id) {
						console.log(this.state.budgetArr[i].id);
						this.state.budgetArr[i].name = this.state.name
						this.state.budgetArr[i].date = this.state.date
						this.state.budgetArr[i].type = this.state.type
						this.state.budgetArr[i].amount = parseInt(this.state.amount);
						this.sumCalc();
					}
				}
			})) :
			(this.setState({[e.target.name]: e.target.value}, () => {
				for (let i = 0; i < this.state.budgetArr.length; i++) {
					if (this.state.id === this.state.budgetArr[i].id) {
						console.log(this.state.budgetArr[i].id);
						this.state.budgetArr[i].name = this.state.name
						this.state.budgetArr[i].date = this.state.date
						this.state.budgetArr[i].type = this.state.type
						this.state.budgetArr[i].amount = parseInt(this.state.amount);
						this.sumCalc();
					}
				}
			}))


	}

	//clr form
	handleClear(e) {
		e.preventDefault();
		this.setState({
			formaction: "new",
			type: "debit",
			date: "",
			name: "",
			amount: "",
			id: null
		})
	}

	//filtering
	handleDateFilert(e) {
		const date = e.target.value;
		this.setState({datefilter: date});
		let {budgetArr} = this.state;
		if (date.length > 0) {
			let newbudgetArr = [];
			for (let i = 0; i < budgetArr.length; i++) {
				if (moment(date).isSame(budgetArr[i].date)) {
					newbudgetArr.push(budgetArr[i]);
				}
			}
			if (newbudgetArr.length > 0) {
				this.setState({budgetArr: newbudgetArr}, () => {
					this.sumCalc()
				})
			}
		} else {
			this.getFullBudget();
		}
	}


	render() {
		let {title, budgetArr, summ} = this.state;
		const divStyle = {
			margin: '20px auto',
		};
		return (
			<div className="App">
				<header className="App-header">
					<img src={logo} className="App-logo" alt="logo"/>
					<h1 className="App-title">{title}</h1>
				</header>

				<form ref="allItems" action="#" style={divStyle}>
					<b>ADD/EDIT </b>
					<select name="type" ref="type" id="type" onChange={this.handleChangeLine.bind(this)} value={this.state.type}>
						<option value="debit">debit</option>
						<option value="credit">credit</option>
					</select>
					<input
						type="text" name="date" ref="date"
						required="required"
						placeholder="date (MM/DD/YYYY)"
						value={this.state.date || ""} onChange={this.handleChangeLine.bind(this)}
					/>
					<input
						type="text" name="name" ref="name"
						required="required"
						placeholder="name"
						value={this.state.name || ""} onChange={this.handleChangeLine.bind(this)}
					/>
					<input
						type="text" name="amount" ref="amount"
						required="required"
						placeholder="amount"
						value={this.state.amount || ""} onChange={this.handleChangeLine.bind(this)}/>
					<button onClick={this.handleAddNewLine.bind(this)}>Save</button>
					<button onClick={this.handleClear.bind(this)}>Clear</button>
				</form>

				<div style={divStyle}>
					<b>FILTER </b>
					<input
						type="text"
						name="datefiter"
						placeholder="filter date (MM/DD/YYYY)"
						onChange={this.handleDateFilert.bind(this)}
						value={this.state.datefilter}
					/>
				</div>

				<ul>
					{budgetArr.map(line =>
						<li key={line.id} style={{listStyleType: "none"}}>
							{line.type}&nbsp;-&nbsp;
							<Moment format="MM/DD/YYYY">{line.date}</Moment>&nbsp;-&nbsp;
							{line.name}
							{line.type == "debit" ? (" + ") : (" - ")}
							{accounting.formatMoney(line.amount, {symbol: "€", precision: 0, thousand: " ", format: "%v%s"})}
							&nbsp;
							<button href="#" onClick={this.handleRemoveLine.bind(this, line.id)}>remove</button>
							&nbsp;
							<button href="#" onClick={this.handleEditLine.bind(this, line.id)}>edit</button>
						</li>
					)}
				</ul>
				<hr/>
				<b>TOTAL:</b>&nbsp;
				{accounting.formatMoney(summ, {symbol: "€", precision: 0, thousand: " ", format: "%v%s"})}

			</div>
		);
	}
}

export default App;
