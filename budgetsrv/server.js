let express = require('express');
let bodyParser = require('body-parser');
let morgan= require('morgan');
let pg = require('pg');
let cors = require('cors');
const PORT = 3000;

let pool = new pg.Pool({
	port:5432,
	database:'budget',
	host: 'localhost'

})
/*pool.connect()
pool.query('SELECT * FROM budget', (err, res) => {
		console.log(err, res)
	pool.end()
	})*/
let app = express();


app.use(bodyParser.json());
app.use(cors())
app.use(bodyParser.urlencoded({extended: true }));
app.use(morgan('dev'));

app.use(function(request, response, next) {
	response.header("Access-Control-Allow-Origin", "*");
	response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});



// Get All data

app.delete('/api/remove/:id',function (request,response) {

	//console.log(request.params.id)
	const id = request.params.id

	pool.connect((err,db,done)=>{
		db.query('DELETE FROM budget WHERE budget.id = $1',[Number(id)], (err, res) => {
			done();
			//console.log(err, res)
			if(err){
				return console.log("DELETE FROM budget  - ERROR "+err );
				//db.end();
				return response.status(400).send({message:err});
			}else{
				console.log("DELETE FROM budget  - OK ");
				//console.log(res.rows);
				//db.end();
				return response.status(200).send({message:"Data removed!"})
			}

		})

	});
})
app.put('/api/editline/:id',function (request,response) {

	//console.log(request.params.id)
	const name = request.body.name;
	const type = request.body.type;
	const date = request.body.date;
	const amount = request.body.amount;
	const id = request.body.id;
	const values = [name,type,date,amount,id];
	console.log(values );

	pool.connect((err,db,done)=>{

		//type, date , name , amount,
		db.query('UPDATE budget SET name = $1, type = $2, date = $3, amount = $4  WHERE id = $5',[...values], (err, res) => {
			done();
			//console.log(err, res)
			if(err){
				return console.log("UPDATE budget  - ERROR "+err );
				//db.end();
				return response.status(400).send({message:err});
			}else{
				console.log("UPDATE budget  - OK ");
				//console.log(res.rows);
				//db.end();
				return response.status(200).send({message:"Data updated!"})
			}

		})

	});
})



app.get('/api/getData',function (request,response) {

	pool.connect((err,db,done)=>{
		db.query('SELECT * FROM budget', (err, res) => {
			done();
			//console.log(err, res)
			if(err){
				console.log("SELECT * FROM budget - ERROR "+err );
				//db.end();
				return response.status(400).send({message:err});
			}else{
				console.log("SELECT * FROM budget - OK ");
				//console.log(res.rows);
				//db.end();
				return response.status(200).send(res.rows)
			}

		})

	});
})


//Add New Line
app.post('/api/newline',function (request,response) {
	//console.log(request.body);
	const type =  request.body.type;
	const date =  request.body.date;
	const name =  request.body.name;
	const amount =  request.body.amount;
	const id = request.body.id;
	const values = [type,date,name,amount,id];

	pool.connect((err,db,done)=>{

		db.query('INSERT INTO budget (type, date , name , amount, id) VALUES ($1,$2,$3,$4,$5)',[...values], (err, res) => {
			done();
			//console.log(err, res)
			if(err){
				console.log("INSERT INTO budget - ERROR "+err);
				//db.end();
				return response.status(400).send({message:+err});
			}else{
				console.log("INSERT INTO budget - OK ");
				//db.end();
				response.status(201).send({message:"Data inserted!"})
			}

		})

	});
})


app.listen(PORT,()=> console.log('PORT: ' +PORT ) )